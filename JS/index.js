/**
 * Bài 1: Tính ngày hôm qua và ngày mai
 * Input: nhập ngày tháng năm
 * Step: Chia ra ba trường hợp lớn TH1: những tháng có 31 ngày, TH2: những tháng có 30 ngày, TH3: là tháng 2 có 28 ngày
 *      Step1: Gán điều kiện ngày tháng năm không được bé hơn bằng 0, số ngày trong một tháng
 *      Step2: Xử lý function tính ngày hôm qua, lưu ý ngày đặc biệt là ngày đầu tháng của ba trường hợp(VD:nếu là ngày 1/3 thì ngày trước đó phải là 28/2)
 *      Step3: xử lý function tính ngày mai, tương tự chỉ khác nếu là ngày 28/2 thì ngày hôm sau phải là 1/3
 *      Lưu ý ngày đầu năm và cuối năm
 * Output: Ngày tháng năm đã được tính toán
 */

function handle_5_hom_qua() {
  var ngayEx5 = +document.getElementById("ngay-ex5").value;
  var thangEx5 = +document.getElementById("thang-ex5").value;
  var namEx5 = +document.getElementById("nam-ex5").value;
  var result5El = document.getElementById("result5");
  if (ngayEx5 <= 0 || thangEx5 <= 0 || namEx5 <= 0) {
    result5El.innerHTML = `<h1> Ngày không hợp lệ </h1>`;
  } else if (ngayEx5 > 28 && thangEx5 == 2) {
    result5El.innerHTML = `<h1> Ngày không hợp lệ </h1>`;
  } else {
    if (
      thangEx5 == 1 ||
      thangEx5 == 3 ||
      thangEx5 == 5 ||
      thangEx5 == 7 ||
      thangEx5 == 8 ||
      thangEx5 == 10 ||
      thangEx5 == 12
    ) {
      if (ngayEx5 > 31) {
        result5El.innerHTML = `<h1> Ngày không hợp lệ </h1>`;
        return 0;
      } else if (ngayEx5 == 1) {
        if (thangEx5 == 1) {
          ngayEx5 = 31;
          thangEx5 = 12;
          namEx5--;
          result5El.innerHTML = `<h1> ${ngayEx5} / ${thangEx5} / ${namEx5} </h1>`;
          return 0;
        } else if (thangEx5 == 3) {
          ngayEx5 = 28;
          thangEx5--;
          result5El.innerHTML = `<h1> ${ngayEx5} / ${thangEx5} / ${namEx5} </h1>`;
          return 0;
        } else {
          ngayEx5 = 30;
          thangEx5--;
          result5El.innerHTML = `<h1> ${ngayEx5} / ${thangEx5} / ${namEx5} </h1>`;
          return 0;
        }
      } else {
        ngayEx5--;
        result5El.innerHTML = `<h1> ${ngayEx5} / ${thangEx5} / ${namEx5} </h1>`;
        return 0;
      }
    }

    if (
      thangEx5 == 2 ||
      thangEx5 == 4 ||
      thangEx5 == 6 ||
      thangEx5 == 9 ||
      thangEx5 == 11
    ) {
      if (ngayEx5 > 30) {
        result5El.innerHTML = `<h1> Ngày không hợp lệ </h1>`;
        return 0;
      } else if (ngayEx5 == 1) {
        ngayEx5 = 31;
        thangEx5--;
        result5El.innerHTML = `<h1> ${ngayEx5} / ${thangEx5} / ${namEx5} </h1>`;
        return 0;
      } else {
        ngayEx5--;
        result5El.innerHTML = `<h1> ${ngayEx5} / ${thangEx5} / ${namEx5} </h1>`;
        return 0;
      }
    }
  }
}

function handle_5_ngay_mai() {
  var ngayEx5 = +document.getElementById("ngay-ex5").value;
  var thangEx5 = +document.getElementById("thang-ex5").value;
  var namEx5 = +document.getElementById("nam-ex5").value;
  var result5El = document.getElementById("result5");
  if (ngayEx5 <= 0 || thangEx5 <= 0 || namEx5 <= 0) {
    result5El.innerHTML = `<h1> Ngày không hợp lệ </h1>`;
  } else if (ngayEx5 > 28 && thangEx5 == 2) {
    result5El.innerHTML = `<h1> Ngày không hợp lệ </h1>`;
  } else {
    if (
      thangEx5 == 1 ||
      thangEx5 == 3 ||
      thangEx5 == 5 ||
      thangEx5 == 7 ||
      thangEx5 == 8 ||
      thangEx5 == 10 ||
      thangEx5 == 12
    ) {
      if (ngayEx5 > 31) {
        result5El.innerHTML = `<h1> Ngày không hợp lệ </h1>`;
        return 0;
      } else if (ngayEx5 == 31 && thangEx5 == 12) {
        ngayEx5 = 1;
        thangEx5 = 1;
        namEx5++;
        result5El.innerHTML = `<h1> ${ngayEx5} / ${thangEx5} / ${namEx5} </h1>`;
        return 0;
      } else if (ngayEx5 == 31) {
        ngayEx5 = 1;
        thangEx5++;
        result5El.innerHTML = `<h1> ${ngayEx5} / ${thangEx5} / ${namEx5} </h1>`;
        return 0;
      } else {
        ngayEx5++;
        result5El.innerHTML = `<h1> ${ngayEx5} / ${thangEx5} / ${namEx5} </h1>`;
        return 0;
      }
    }
    if (
      thangEx5 == 2 ||
      thangEx5 == 4 ||
      thangEx5 == 6 ||
      thangEx5 == 9 ||
      thangEx5 == 11
    ) {
      if (ngayEx5 > 30) {
        result5El.innerHTML = `<h1> Ngày không hợp lệ </h1>`;
        return 0;
      } else if (ngayEx5 == 28 && thangEx5 == 2) {
        ngayEx5 = 1;
        thangEx5++;
        result5El.innerHTML = `<h1> ${ngayEx5} / ${thangEx5} / ${namEx5} </h1>`;
        return 0;
      } else if (ngayEx5 == 30) {
        ngayEx5 = 1;
        thangEx5++;
        result5El.innerHTML = `<h1> ${ngayEx5} / ${thangEx5} / ${namEx5} </h1>`;
        return 0;
      } else {
        ngayEx5++;
        result5El.innerHTML = `<h1> ${ngayEx5} / ${thangEx5} / ${namEx5} </h1>`;
        return 0;
      }
    }
  }
}

/**
 * Bài tập 2
 * Input: Tháng và năm cần tính
 * Step: Dùng switch case để ra 3 trường hợp
 *      TH1: Rơi vào những tháng có 31 ngày
 *      TH2: Rơi vào những tháng có 30 ngày
 *      TH3: Tháng 2, dùng if để chia ra năm nhuận(có 29 ngày) và không nhuận(có 28 ngày)
 * Output: Số ngày của tháng đó
 */
function handle_6() {
  var thangEx6 = +document.getElementById("thang-ex6").value;
  var namEx6 = +document.getElementById("nam-ex6").value;

  switch (thangEx6) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      document.getElementById(
        "result6"
      ).innerHTML = ` Tháng ${thangEx6} năm ${namEx6} có 31 ngày `;
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      document.getElementById(
        "result6"
      ).innerHTML = ` Tháng ${thangEx6} năm ${namEx6} có 30 ngày `;
      break;
    case 2:
      if (namEx6 % 4 == 0 && namEx6 % 100 != 0) {
        document.getElementById(
          "result6"
        ).innerHTML = ` Tháng ${thangEx6} năm ${namEx6} có 29 ngày `;
      } else if (namEx6 % 400 == 0) {
        document.getElementById(
          "result6"
        ).innerHTML = ` Tháng ${thangEx6} năm ${namEx6} có 29 ngày `;
      } else {
        document.getElementById(
          "result6"
        ).innerHTML = ` Tháng ${thangEx6} năm ${namEx6} có 28 ngày `;
      }
      break;
    default:
      document.getElementById("result6").innerHTML = ` Tháng năm không hợp lệ `;
      break;
  }
}

/**
 * Bài tập 3
 * Input: Số có 3 chữ số
 *
 * Step:
 * Step 1: Dùng toán tử % để lấy số hàng trăm, chục và đơn vị
 * Step 2: Dùng switch case để quy ước các đọc số hàng trăm, chục và đơn vị
 * Step 3: Dùng if else để chia ra các trường hợp đọc tên (VD:500, 301, 110, 215)
 *
 * Output: Cách gọi tên
 */

function handle_7() {
  var numEx7 = document.getElementById("num-ex7").value * 1;
  var result7El = document.getElementById("result7");
  var hangTram = 0;
  var hangChuc = 0;
  var hangDonVi = 0;
  var hangTramDoc = "";
  var hangChucDoc = "";
  var hangDonViDoc = "";
  hangTram = Math.floor(numEx7 / 100);
  hangChuc = Math.floor((numEx7 % 100) / 10);
  hangDonVi = Math.floor((numEx7 % 100) % 10);
  switch (hangTram) {
    case 1:
      hangTramDoc = "Một";
      break;
    case 2:
      hangTramDoc = "Hai";
      break;
    case 3:
      hangTramDoc = "Ba";
      break;
    case 4:
      hangTramDoc = "Bốn";
      break;
    case 5:
      hangTramDoc = "Năm";
      break;
    case 6:
      hangTramDoc = "Sáu";
      break;
    case 7:
      hangTramDoc = "Bảy";
      break;
    case 8:
      hangTramDoc = "Tám";
      break;
    case 9:
      hangTramDoc = "Chín";
      break;
  }

  switch (hangChuc) {
    case 1:
      hangChucDoc = "Mười";
      break;
    case 2:
      hangChucDoc = "Hai";
      break;
    case 3:
      hangChucDoc = "Ba";
      break;
    case 4:
      hangChucDoc = "Bốn";
      break;
    case 5:
      hangChucDoc = "Năm";
      break;
    case 6:
      hangChucDoc = "Sáu";
      break;
    case 7:
      hangChucDoc = "Bảy";
      break;
    case 8:
      hangChucDoc = "Tám";
      break;
    case 9:
      hangChucDoc = "Chín";
      break;
  }

  switch (hangDonVi) {
    case 1:
      hangDonViDoc = "Một";
      break;
    case 2:
      hangDonViDoc = "Hai";
      break;
    case 3:
      hangDonViDoc = "Ba";
      break;
    case 4:
      hangDonViDoc = "Bốn";
      break;
    case 5:
      hangDonViDoc = "Năm";
      break;
    case 6:
      hangDonViDoc = "Sáu";
      break;
    case 7:
      hangDonViDoc = "Bảy";
      break;
    case 8:
      hangDonViDoc = "Tám";
      break;
    case 9:
      hangDonViDoc = "Chín";
      break;
  }
  if (hangChuc == 0 && hangDonVi != 0) {
    result7El.innerHTML = `<h1> ${hangTramDoc} Lẻ ${hangDonViDoc} </h1>`;
  } else if (hangChuc != 0 && hangDonVi == 0) {
    result7El.innerHTML = `<h1> ${hangTramDoc} Trăm ${hangChucDoc} </h1>`;
  } else if (hangChuc == 0 && hangDonVi == 0) {
    result7El.innerHTML = `<h1> ${hangTramDoc} Trăm </h1>`;
  } else if (hangChuc == 1 && hangDonVi != 0) {
    result7El.innerHTML = `<h1> ${hangTramDoc} Trăm ${hangChucDoc} ${hangDonViDoc} </h1>`;
  } else {
    result7El.innerHTML = `<h1> ${hangTramDoc} Trăm ${hangChucDoc} Mươi ${hangDonViDoc} </h1>`;
  }
}

/**
 * Bài tập 4:
 * Input: Tọa độ của ba sinh viên và trường, tên ba sinh viên
 *
 * Step: Để tính khoảng cách của hai điểm A(x1;y1) và B(x2;y2) ta dùng công thức: AB^2 = (x2-x1)^2 + (y2-y1)^2
 * Tuy nhiên để bài toán đơn giản hơn ta chỉ cần so sánh bình phương khoảng cách của các sinh viên và trường
 *      Step 1: Tính khoảng cách của các sinh viên và trường
 *      Step 2: Dùng Math.max để tìm ra khoảng cách lớn nhất
 *      Step 3: Dùng switch case để chỉ ra tên sinh viên xa trường nhất
 *
 * Output: in ra tên sinh viên xa trường nhất
 */

function handle_8() {
  var result8El = document.getElementById("result8");
  var name1El = document.getElementById("name1").value;
  var name2El = document.getElementById("name2").value;
  var name3El = document.getElementById("name3").value;
  var x1El = +document.getElementById("x1").value;
  var x2El = +document.getElementById("x2").value;
  var x3El = +document.getElementById("x3").value;
  var xTruongEl = +document.getElementById("x4").value;
  var y1El = +document.getElementById("y1").value;
  var y2El = +document.getElementById("y2").value;
  var y3El = +document.getElementById("y3").value;
  var yTruongEl = +document.getElementById("y4").value;
  var d1 =
    (x1El - xTruongEl) * (x1El - xTruongEl) +
    (y1El - yTruongEl) * (y1El - yTruongEl);
  var d2 =
    (x2El - xTruongEl) * (x2El - xTruongEl) +
    (y2El - yTruongEl) * (y2El - yTruongEl);
  var d3 =
    (x3El - xTruongEl) * (x3El - xTruongEl) +
    (y3El - yTruongEl) * (y3El - yTruongEl);

  if (d1 == d2 || d1 == d3 || d2 == d3) {
    result8El.innerHTML = `<h1> Sinh viên không được ở cùng một địa điểm </h1>`;
    return 0;
  }
  switch (Math.max(d1, d2, d3)) {
    case d1:
      result8El.innerHTML = `<h1> Sinh viên ${name1El} xa trường nhất </h1>`;
      break;
    case d2:
      result8El.innerHTML = `<h1> Sinh viên ${name2El} xa trường nhất </h1>`;
      break;
    case d3:
      result8El.innerHTML = `<h1> Sinh viên ${name3El} xa trường nhất </h1>`;
      break;
  }
}
